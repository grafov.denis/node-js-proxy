const net = require('net');

const proxy = net.createServer();

proxy.listen(8080);


function parseResponse(response) {
  const lines = response.split('\r\n');
  const headers = new Map();
  let head = "";
  let body = "";
  let bodyStarted = false;
  for (i = 0; i < lines.length; i++) {
    if (lines[i] === "") {
      bodyStarted = true;
      continue;
    }
    if (!bodyStarted) {
      head += lines[i] + "\r\n";
      const splitted = lines[i].split(': ');
      if (splitted[0] != null && splitted[1] != null) {
        headers.set(splitted[0], splitted[1]);
      }
    } else {
      body += lines[i];
    }
  }
  return {
    headers: headers,
    head, head,
    body: body
  };
}


proxy.on("connection", proxy_socket => {
  proxy_socket.on('data', request => {
    const _data = request.toString();
    const _dataList = _data.split("\r\n");
    console.log(_dataList[0]);

    const headers = new Map();
    _dataList.forEach(line => {
      const splitted = line.split(': ');
      headers.set(splitted[0], splitted[1]);
    });


    function writeHeader(header) {
      if (headers.has(header) && headers.get(header) != undefined) {
        return `${header}: ${headers.get(header)}\n`;
      } else {
        return '';
      }
    }

    let _request = _dataList[0] + "\r\n";
    _request += writeHeader("Host");
    _request += writeHeader("User-Agent");
    _request += writeHeader("Accept");
    _request += writeHeader("Accept-Language");
    _request += writeHeader("Accept-Encoding");
    _request += writeHeader("Referer");
    _request += writeHeader("Content-Type");
    _request += writeHeader("Content-Length");
    _request += writeHeader("Connection");
    _request += writeHeader("Upgrade-Insecure-Requests");
    _request += writeHeader("Cache-Control");
    _request += "\r\n";

    // const client = new net.Socket();
    const client =
      net.createConnection(80, headers.get("Host"), () => {
        client.write(_request);
      });
    // console.log(headers);

    var head;
    let contentLength;
    var currentLength = 0;
    var readHead = true;
    var body = "";

    client.on('data', buffer => {
      if (readHead) {
        const response = parseResponse(buffer.toString());
        head = response.head;
        body = response.body;
        contentLength = response.headers.get('Content-Length');
        currentLength = Buffer.from(response.body).length;
        readHead = false;
      } else {
        currentLength += buffer.length;
        body += buffer.toString();
      }
      if (currentLength == contentLength) {
        readHead = true;
        currentLength = 0;
        contentLength = 0;
        // fs.writeFileSync('index.html', head + "\r\n" + body);
        const result = Buffer.from(head + "\r\n" + body, 'utf-8');
        proxy_socket.bufferSize = request.length;
        proxy_socket.write(result, () => {
          client.destroy();
          proxy_socket.destroy();
        });
      }
      // proxy_socket.write(buffer);
    });

    client.on('error', error => {
      console.log(`client err: ${error}`);
    });
  });
});

proxy.on('error', error => {
  console.log(`proxy err: ${error}`);
})